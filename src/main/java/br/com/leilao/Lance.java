package br.com.leilao;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Lance {

    private Usuario usuario;
    private Double valorDoLance;

    public Lance() {}

    public Lance(Usuario usuario, Double valorDoLance) {
        this.usuario = usuario;
        this.valorDoLance = valorDoLance;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Double getValorDoLance() {
        return valorDoLance;
    }

    public void setValorDoLance(Double valorDoLance) {
        BigDecimal valorDoLanceBg = new BigDecimal(valorDoLance).setScale(2, RoundingMode.HALF_EVEN);

        this.valorDoLance = valorDoLanceBg.doubleValue();
    }
}
