package br.com.leilao;

import java.util.ArrayList;
import java.util.List;

public class Leilao {
    private List<Lance> lances = new ArrayList<Lance>();

    public Leilao() {
    }

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    public void adicionaLance(Lance lance) {
        if (lances.size() > 0) {
            for (int i = 0; i < lances.size(); i++) {
                if (lance.getValorDoLance() < lances.get(i).getValorDoLance()) {
                    throw new RuntimeException("O valor do lance não pode ser inferior ao de um lance dado anteriormente.");
                }else if (lance.getValorDoLance().equals(lances.get(i).getValorDoLance())){
                    throw new RuntimeException("O valor do lance não pode ser igual à um já dado anteriormente.");
                }
            }
        }
        lances.add(lance);
    }
}
