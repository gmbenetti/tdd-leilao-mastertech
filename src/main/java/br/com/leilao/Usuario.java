package br.com.leilao;

public class Usuario {

    private String nome;
    private Integer id = 0;

    public Usuario(String nome) {
        this.id += this.getId();
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
