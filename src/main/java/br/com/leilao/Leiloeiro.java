package br.com.leilao;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Leiloeiro {

    private String nome;
    private Leilao leilao = new Leilao();

    public Leiloeiro(String nome, Leilao leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public String retornaNomeUsuarioMaiorLance() {

        Lance lanceMaior = new Lance();
        lanceMaior = retornaMaiorLance();

        return lanceMaior.getUsuario().getNome();
    }

    public Double retornaValorMaiorLance() {

        Lance lanceMaior = new Lance();
        lanceMaior = retornaMaiorLance();

        BigDecimal valorLance = new BigDecimal(lanceMaior.getValorDoLance()).setScale(2, RoundingMode.HALF_EVEN);

        return valorLance.doubleValue();
    }

    private Lance retornaMaiorLance(){
        if (this.leilao.getLances().size() > 0) {
            Lance lance = new Lance();
            Double valorLance = this.leilao.getLances().get(0).getValorDoLance();

            for (int i = 0; i < this.leilao.getLances().size(); i++) {
                if(this.leilao.getLances().get(i).getValorDoLance() > valorLance) {
                    valorLance = this.leilao.getLances().get(i).getValorDoLance();
                    lance = this.leilao.getLances().get(i);
                }
            }
            return lance;
        }else{
            throw new RuntimeException("O leilão ainda não possui nenhum lance.");
        }
    }
}
