package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LeiloeiroTeste {

    private Leiloeiro leiloeiro;
    private Leilao leilao;

    @BeforeEach
    public void setUp() {
        leilao = new Leilao();
        leiloeiro = new Leiloeiro("Manfred", leilao);
    }

    @Test
    public void testaRetornoMaiorLance() {

        Usuario usuario1 = new Usuario("Jose");
        Lance lance1 = new Lance(usuario1, 750.00);

        Usuario usuario2 = new Usuario("Aroldo");
        Lance lance2 = new Lance(usuario2, 800.00);

        Usuario usuario3 = new Usuario("Zaqueu");
        Lance lance3 = new Lance(usuario3, 900.00);

        leilao.adicionaLance(lance1);
        leilao.adicionaLance(lance2);
        leilao.adicionaLance(lance3);

        Assertions.assertEquals("Zaqueu", leiloeiro.retornaNomeUsuarioMaiorLance());
        Assertions.assertEquals(900.00, leiloeiro.retornaValorMaiorLance());
    }

    @Test
    public void testaAdicionarSegundoLanceMenor(){

        Usuario usuario1 = new Usuario("Jose");
        Lance lance1 = new Lance(usuario1, 750.00);

        Usuario usuario2 = new Usuario("Aroldo");
        Lance lance2 = new Lance(usuario2, 800.00);

        Assertions.assertThrows(RuntimeException.class, () -> { leiloeiro.retornaNomeUsuarioMaiorLance(); });
    }
}
