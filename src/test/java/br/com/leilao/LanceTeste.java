package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LanceTeste {

    @Test
    public void testaCriarLance(){
        Usuario usuario = new Usuario("Joao");
        Lance lance = new Lance(usuario, 1000.00);

        Assertions.assertEquals("Joao", lance.getUsuario().getNome());
        Assertions.assertEquals(1000.00, lance.getValorDoLance());
    }

}
