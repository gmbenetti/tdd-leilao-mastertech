package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LeilaoTeste {
    private Usuario usuario1 = new Usuario("Jose");
    private Lance lance1 = new Lance(usuario1, 750.00);
    private Leilao leilao;

    @BeforeEach
    public void setUp() {
        leilao = new Leilao();
    }

    @Test
    public void testaAdicionarLanceAoLeilao(){
        leilao.adicionaLance(lance1);

        Assertions.assertEquals("Jose", leilao.getLances().get(0).getUsuario().getNome());
        Assertions.assertEquals(750.00, leilao.getLances().get(0).getValorDoLance());
    }

    @Test
    public void testaAdicionarSegundoLanceMaior(){
        leilao.adicionaLance(lance1);

        Usuario usuario2 = new Usuario("Aroldo");
        Lance lance2 = new Lance(usuario2, 800.00);

        leilao.adicionaLance(lance2);

        Assertions.assertEquals("Aroldo", leilao.getLances().get(1).getUsuario().getNome());
        Assertions.assertEquals(800.00, leilao.getLances().get(1).getValorDoLance());
    }

    @Test
    public void testaAdicionarSegundoLanceMenor(){
        leilao.adicionaLance(lance1);

        Usuario usuario2 = new Usuario("Aroldo");
        Lance lance2 = new Lance(usuario2, 700.00);

        Assertions.assertThrows(RuntimeException.class, () -> {leilao.adicionaLance(lance2);});
    }

    @Test
    public void testaAdicionarSegundoLanceIgualAnterior(){
        leilao.adicionaLance(lance1);

        Usuario usuario2 = new Usuario("Aroldo");
        Lance lance2 = new Lance(usuario2, 750.00);

        Assertions.assertThrows(RuntimeException.class, () -> {leilao.adicionaLance(lance2);});
    }
}
